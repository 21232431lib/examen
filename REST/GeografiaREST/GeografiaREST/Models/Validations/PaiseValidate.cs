﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GeografiaREST.Models
{
    [MetadataType(typeof(Paise.MetaData))]
    public partial class Paise
    {
        sealed class MetaData
        {
            [Key]
            public int Paid;

            [Required(ErrorMessage = "Ingresa el nombre del pais")]
            public string Paname;

            [Required(ErrorMessage = "Ingresa la capital del pais")]
            public string Pacapital;

            [Required(ErrorMessage = "Ingresa el continente del pais")]
            public string Continente;

            [Required(ErrorMessage = "Ingresa el numero de habitantes del pais")]
            public Nullable<int> Habitantes;
        }
    }
}