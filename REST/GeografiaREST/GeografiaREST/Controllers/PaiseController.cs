﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GeografiaREST.Models;

namespace GeografiaREST.Controllers
{
    public class PaiseController : ApiController
    {
        private PAISEntities db = new PAISEntities();

        // GET: api/Paises
        public IQueryable<Paise> GetPaise()
        {
            return db.Paise;
        }

        // GET: api/Paises/5
        [ResponseType(typeof(Paise))]
        public IHttpActionResult GetPaise(int id)
        {
            Paise paise = db.Paise.Find(id);
            if (paise == null)
            {
                return NotFound();
            }

            return Ok(paise);
        }

        // PUT: api/Paises/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPaise(int id, Paise paise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != paise.Paid)
            {
                return BadRequest();
            }

            db.Entry(paise).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Paises
        [ResponseType(typeof(Paise))]
        public IHttpActionResult PostPaise(Paise paise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Paise.Add(paise);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = paise.Paid }, paise);
        }

        // DELETE: api/Paises/5
        [ResponseType(typeof(Paise))]
        public IHttpActionResult DeletePaise(int id)
        {
            Paise paise = db.Paise.Find(id);
            if (paise == null)
            {
                return NotFound();
            }

            db.Paise.Remove(paise);
            db.SaveChanges();

            return Ok(paise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PaiseExists(int id)
        {
            return db.Paise.Count(e => e.Paid == id) > 0;
        }
    }
}