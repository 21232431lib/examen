﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Geografia.Models
{
    public class Paise
    {
        [Key]
        [Display(Name = "Indice")]
        public int Paid { get; set; }

        [Required(ErrorMessage = "Ingresa nombre")]
        [Display(Name = "Nombre del pais")]
        public string Paname { get; set; }

        [Required(ErrorMessage = "Ingresa capital")]
        [Display(Name = "Capital")]
        public string Pacapital { get; set; }

        [Required(ErrorMessage = "Ingresa continente")]
        [Display(Name = "Continente")]
        public string Continente { get; set; }

        [Required(ErrorMessage = "Ingresa numero de habitantes")]
        [Display(Name = "Numero de habitantes")]
        public int Habitantes { get; set; }

   
    }
}
