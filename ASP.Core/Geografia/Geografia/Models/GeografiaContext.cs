﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Geografia.Models
{
    public class GeografiaContext : DbContext
    {
        public GeografiaContext(DbContextOptions<GeografiaContext> options) : base(options)
        {
        }
        public DbSet<Paise> Paise { get; set; }
    }
}
