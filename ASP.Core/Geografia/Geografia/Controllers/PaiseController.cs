﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geografia.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Geografia.Controllers
{
    public class PaiseController : Controller
    {
        /*Varibale para accder a base de datos*/
        private readonly GeografiaContext _db;

        /*Constructor*/
        public PaiseController(GeografiaContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Paise.ToList();
            return View(displaydata);

        }

        [HttpGet]
        public async Task<IActionResult> Index(string PaiSearch)
        {
            ViewData["GetPaiseDetail"] = PaiSearch;
            var Cliquery = from x in _db.Paise select x;
            if (!String.IsNullOrEmpty(PaiSearch))
            {
                Cliquery = Cliquery.Where(x => x.Paname.Contains(PaiSearch) ||
                x.Pacapital.Contains(PaiSearch));
            }
            return View(await Cliquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Paise nPai)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nPai);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nPai);

        }
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getPaiDetail = await _db.Paise.FindAsync(id);
            return View(getPaiDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getPaiDetail = await _db.Paise.FindAsync(id);
            return View(getPaiDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Paise oldPai)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldPai);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldPai);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getPaiDetail = await _db.Paise.FindAsync(id);
            return View(getPaiDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getPaiDetail = await _db.Paise.FindAsync(id);
            _db.Paise.Remove(getPaiDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}
